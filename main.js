document.write("<h1>Dice Rollx1000 Text Results (two die)</h1>");

function rollDie(min = 2, max = 12){
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; 
}

let counts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
let results = [];

for (let count=0; count < 1000; count++) {
    let result = rollDie()
    counts[result] = counts[result] + 1
    results.push(result)

}

console.log(results);
console.log(counts);


document.write("2: " + counts[2]);
document.write("<br>3: " + counts[3]);
document.write("<br>4: " + counts[4]);
document.write("<br>5: " + counts[5]);
document.write("<br>6: " + counts[6]);
document.write("<br>7: " + counts[7]);
document.write("<br>8: " + counts[8]);
document.write("<br>9: " + counts[9]);
document.write("<br>10: " + counts[10]);
document.write("<br>11: " + counts[11]);
document.write("<br>12: " + counts[12]);

document.write("<br><br><h1>Graph Results</h1>");

function rect(height, width, color){
    return `<div style=" height: ${height}px; width: ${width}mm; background-color: ${color}"></div>`
}

document.write("<h2>2: </h2>" + rect(20, counts[2], 'purple'))
document.write("<h2>3: </h2>" + rect(20, counts[3], 'purple'))
document.write("<h2>4: </h2>" + rect(20, counts[4], 'purple'))
document.write("<h2>5: </h2>" + rect(20, counts[5], 'purple'))
document.write("<h2>6: </h2>" + rect(20, counts[6], 'purple'))
document.write("<h2>7: </h2>" + rect(20, counts[7], 'purple'))
document.write("<h2>8: </h2>" + rect(20, counts[8], 'purple'))
document.write("<h2>9: </h2>" + rect(20, counts[9], 'purple'))
document.write("<h2>10: </h2>" + rect(20, counts[10], 'purple'))
document.write("<h2>11: </h2>" + rect(20, counts[11], 'purple'))
document.write("<h2>12: </h2>" + rect(20, counts[12], 'purple'))